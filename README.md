# Initiating connection to mongo database

следующие команды были протестированы для системы macOS  

0. Скачивание репозитория
```
git clone https://gitlab.com/emnigma/mongo-test.git 
cd mongo_test
```

1. Создание виртуальной среды
```
python3 -m venv .mongo-test
```

2. Активация виртуальной среды
```
source ./.mongo_test/bin/activate
```

3. Скачивание зависимостей
```
pip install -r requirements.txt
```

4. Скачивание и запуск mongo
(Инструкция для разных систем: https://docs.mongodb.com/manual/administration/install-community/)   
запуск/остановка локальной БД на платформе macOS как сервис brew:
```
brew services start mongodb-community
```
```
brew services stop mongodb-community
```

5. Скачивание и запуск mongoDB Compass(опционально)
https://www.mongodb.com/try/download/compass

6. Инициализация локальной базы данныx
(С помощью Compass: создать новую базу данных, назвать 'mydb', создать в ней коллекцию 'sample', поместить в нее [образец](./db_struct.json) в соответствии с инструкциями)

Образцы можно получить из ответа app/app.py на запросы вида
```
http://<ip adress>:<port>/createReport?image_path=<full path to image>&method=2
```