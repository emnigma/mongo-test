from pymongo import MongoClient
import json

# TODO: on post: save image on device, post metadata to db,
# on get - get matadata, image from db


client = MongoClient('localhost')

for db in client.list_databases():
    print(db)

db = client.mydb

query = db.sample.find()

for item in query:
    print(item)

client.close()
